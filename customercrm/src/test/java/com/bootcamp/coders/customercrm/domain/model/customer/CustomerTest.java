package com.bootcamp.coders.customercrm.domain.model.customer;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CustomerTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getId() {
    }

    @Test
    void getDocumentType() {
    }

    @Test
    void getDocument() {
    }

    @Test
    void getFullName() {
    }

    @Test
    void getBirthDate() {
    }

    @Test
    void getDocumentIssueDate() {
    }

    @Test
    void getEmail() {
    }

    @Test
    void getCellphone() {
    }
}