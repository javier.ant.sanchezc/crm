package com.bootcamp.coders.customercrm.domain.service;

import com.bootcamp.coders.customercrm.domain.model.customer.Customer;
import com.bootcamp.coders.customercrm.domain.port.repository.CustomerRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SearchCustomerService {
    private final CustomerRepository repository;

    public SearchCustomerService(CustomerRepository repository) {
        this.repository = repository;
    }

    public List<Customer> getAllClients() {
        return this.repository.getAll();
    }
}
