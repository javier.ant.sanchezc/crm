package com.bootcamp.coders.customercrm.domain.model.customer.repository;


import com.bootcamp.coders.customercrm.domain.model.customer.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepositoryJPA extends JpaRepository<Customer, Long> {
    boolean existsCustomerByDocument(String document);
}
