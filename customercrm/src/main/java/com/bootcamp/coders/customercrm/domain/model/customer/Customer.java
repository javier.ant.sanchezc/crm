package com.bootcamp.coders.customercrm.domain.model.customer;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Data
@NoArgsConstructor
@Table(name = "customers")
public class Customer {

    @Id
    @GeneratedValue
    private Long id;
    private String documentType;
    private String document;
    private String fullName;
    private LocalDate birthDate;
    private LocalDate documentIssueDate;
    private String email;
    private String cellphone;

    public Customer(String documentType, String document, String fullName, LocalDate birthDate, LocalDate documentIssueDate, String email, String cellphone) {
        this.documentType = documentType;
        this.document = document;
        this.fullName = fullName;
        this.birthDate = birthDate;
        this.documentIssueDate = documentIssueDate;
        this.email = email;
        this.cellphone = cellphone;
    }
}
