package com.bootcamp.coders.customercrm.application.factory;

import com.bootcamp.coders.customercrm.domain.model.customer.Customer;
import com.bootcamp.coders.customercrm_openapiv3.project.api.model.CustomerInfo;
import org.springframework.stereotype.Component;

@Component
public class CustomerFactory {

    public Customer create(CustomerInfo info) {
        return new Customer(
                info.getDocumentType(),
                info.getDocument(),
                info.getName() + " " + info.getLastName(),
                info.getBirthDate(),
                info.getDocumentIssueDate(),
                info.getEmail(),
                info.getCellphone()
        );
    }
}
