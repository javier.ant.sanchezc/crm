package com.bootcamp.coders.customercrm.application.service;

import com.bootcamp.coders.customercrm.domain.service.RemoveCustomerService;
import org.springframework.stereotype.Service;

@Service
public class RemoveCustomerApplicationService {

    private final RemoveCustomerService service;

    public RemoveCustomerApplicationService(RemoveCustomerService service) {
        this.service = service;
    }

    public boolean execute(Long id) {
        return this.service.removeClient(id);
    }
}
