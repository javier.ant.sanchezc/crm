package com.bootcamp.coders.customercrm.domain.service;

import com.bootcamp.coders.customercrm.domain.model.customer.Customer;
import com.bootcamp.coders.customercrm.domain.port.repository.CustomerRepository;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AddCustomerService {

    @Autowired
  CustomerRepository repository;
    final static Log logger = LogFactory.getLog(CustomerRepository.class);

    public AddCustomerService(CustomerRepository repository) {
        try {
            this.repository = repository;
        }
        catch (Exception e)
        {
            logger.error("Falla en le asginacion  "+e);
        }

    }

    public Long registerClient(Customer customer) {
        logger.info("Registro del cliente");

        return this.repository.registerClient(customer);
    }
}
