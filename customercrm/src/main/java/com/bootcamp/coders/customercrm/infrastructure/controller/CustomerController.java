package com.bootcamp.coders.customercrm.infrastructure.controller;


import com.bootcamp.coders.customercrm.application.service.AddCustomerApplicationService;
import com.bootcamp.coders.customercrm.application.service.RemoveCustomerApplicationService;
import com.bootcamp.coders.customercrm.application.service.SearchCustomerApplicationService;
import com.bootcamp.coders.customercrm.domain.model.customer.Customer;
import com.bootcamp.coders.customercrm_openapiv3.project.api.CustomerCrmApi;
import com.bootcamp.coders.customercrm_openapiv3.project.api.model.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

@RestController
public class CustomerController implements CustomerCrmApi {
    final static Log logger = LogFactory.getLog(CustomerController.class);

    private final AddCustomerApplicationService addCustomerApplicationService;
    private final RemoveCustomerApplicationService removeCustomerApplicationService;
    private final SearchCustomerApplicationService searchCustomerApplicationService;


    public CustomerController(AddCustomerApplicationService addCustomerApplicationService,
                              RemoveCustomerApplicationService removeCustomerApplicationService,
                              SearchCustomerApplicationService searchCustomerApplicationService) {
        this.addCustomerApplicationService = addCustomerApplicationService;
        this.removeCustomerApplicationService = removeCustomerApplicationService;
        this.searchCustomerApplicationService = searchCustomerApplicationService;
    }

    @Override
    public ResponseEntity<CRMConsultResponse> getAllClients() {
        List<Customer> clients = this.searchCustomerApplicationService.execute();
        CRMConsultResponse response = new CRMConsultResponse().data(Collections.singletonList(clients));
        try {
            logger.info("Asignacion de la peticion");

        }
       catch (Exception e)
       {
           logger.error("Error en la peticion "+e);
       }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<CRMRegisterResponse> registerClient(CustomerInfo customerInfo) {
        Long customerId = this.addCustomerApplicationService.execute(customerInfo);
        CRMRegisterResponse response = new CRMRegisterResponse().data(new CRMRegisterResponseData().id(customerId));
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<CRMDeleteResponse> removeClient(Long id) {
        boolean removed = this.removeCustomerApplicationService.execute(id);
        CRMDeleteResponse response = new CRMDeleteResponse().data(new CRMDeleteResponseData().removed(removed));
        try {
            logger.info("Asignacion de la peticion");
        }
        catch (Exception e)
        {
            logger.error("Error en la peticion "+e);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
