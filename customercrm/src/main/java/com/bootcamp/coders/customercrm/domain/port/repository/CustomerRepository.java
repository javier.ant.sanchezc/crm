package com.bootcamp.coders.customercrm.domain.port.repository;


import com.bootcamp.coders.customercrm.domain.model.customer.Customer;

import java.util.List;

public interface CustomerRepository {
    Long registerClient(Customer customer);
    List<Customer> getAll();
    boolean removeClient(Long id);
}
