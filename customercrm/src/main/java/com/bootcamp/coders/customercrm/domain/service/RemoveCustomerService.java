package com.bootcamp.coders.customercrm.domain.service;

import com.bootcamp.coders.customercrm.domain.port.repository.CustomerRepository;
import org.springframework.stereotype.Service;

@Service
public class RemoveCustomerService {
    private final CustomerRepository repository;

    public RemoveCustomerService(CustomerRepository repository) {
        this.repository = repository;
    }

    public boolean removeClient(Long id) {
        return this.repository.removeClient(id);
    }
}
