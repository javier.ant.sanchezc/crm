package com.bootcamp.coders.customercrm.application.service;

import com.bootcamp.coders.customercrm.application.factory.CustomerFactory;
import com.bootcamp.coders.customercrm.domain.model.customer.Customer;
import com.bootcamp.coders.customercrm.domain.service.AddCustomerService;
import com.bootcamp.coders.customercrm.infrastructure.controller.CustomerController;
import com.bootcamp.coders.customercrm_openapiv3.project.api.model.CustomerInfo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

@Service
public class AddCustomerApplicationService {
    final static Log logger = LogFactory.getLog(AddCustomerApplicationService.class);

    private final AddCustomerService service;
    private final CustomerFactory factory;

    public AddCustomerApplicationService(AddCustomerService service, CustomerFactory factory) {
        this.service = service;
        this.factory = factory;
    }

    public Long execute(CustomerInfo customerInfo) {
        Customer customer = this.factory.create(customerInfo);
        try {
            logger.info("Seguimiento  de la peticion");
            this.service.registerClient(customer);
        }
        catch (Exception e)
        {
            logger.error("Error en la peticion "+e);
        }

        return this.service.registerClient(customer);
    }
}
