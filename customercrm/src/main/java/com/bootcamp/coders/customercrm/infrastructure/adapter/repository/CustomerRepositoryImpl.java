package com.bootcamp.coders.customercrm.infrastructure.adapter.repository;


import com.bootcamp.coders.customercrm.domain.model.customer.Customer;
import com.bootcamp.coders.customercrm.domain.model.customer.repository.CustomerRepositoryJPA;
import com.bootcamp.coders.customercrm.domain.port.repository.CustomerRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository {
    private final CustomerRepositoryJPA repositoryJPA;

    public CustomerRepositoryImpl(CustomerRepositoryJPA repositoryJPA) {
        this.repositoryJPA = repositoryJPA;
    }

    @Override
    public Long registerClient(Customer customer) {
        if(repositoryJPA.existsCustomerByDocument(customer.getDocument())) {
            return -1L;
        }
        Customer finalCustomer = repositoryJPA.save(customer);
        return finalCustomer.getId();
    }

    @Override
    public List<Customer> getAll() {
        return this.repositoryJPA.findAll();
    }

    @Override
    public boolean removeClient(Long id) {
        if(!this.repositoryJPA.existsById(id)) {
            return false;
        }
        this.repositoryJPA.deleteById(id);
        return true;
    }

}
