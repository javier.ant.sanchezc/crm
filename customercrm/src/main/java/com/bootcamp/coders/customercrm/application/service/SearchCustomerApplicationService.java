package com.bootcamp.coders.customercrm.application.service;

import com.bootcamp.coders.customercrm.domain.model.customer.Customer;
import com.bootcamp.coders.customercrm.domain.service.SearchCustomerService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SearchCustomerApplicationService {

    private final SearchCustomerService service;

    public SearchCustomerApplicationService(SearchCustomerService service) {
        this.service = service;
    }

    public List<Customer> execute() {
        return this.service.getAllClients();
    }
}
