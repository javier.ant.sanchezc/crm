package com.bootcamp.coders.customercrm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomercrmApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomercrmApplication.class, args);
	}

}
